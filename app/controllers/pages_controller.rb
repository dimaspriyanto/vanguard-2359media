class PagesController < ApplicationController
  def index
    if consumer_signed_in?
      @endpoints = Endpoint.all
    elsif owner_signed_in?
      @consumers = Consumer.all
    end
  end

  def update_rate_limit
    consumer = Consumer.find(params[:id])
    consumer.rate_limit = params[:rate_limit].to_i
    if consumer.save
      redirect_to :back, notice: 'Rate limit updated'
    end
  end

end
