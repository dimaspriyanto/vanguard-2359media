class EndpointsController < ApplicationController
  before_action :set_endpoint, only: [:show, :edit, :update, :destroy, :subscribe, :access]
  before_action :authenticate_owner!, except: [:subscribe, :access]
  before_action :authenticate_consumer!, only: [:subscribe, :access]

  respond_to :html

  def index
    @endpoints = Endpoint.all
    respond_with(@endpoints)
  end

  def show
    respond_with(@endpoint)
  end

  def new
    @endpoint = Endpoint.new
    respond_with(@endpoint)
  end

  def edit
  end

  def create
    @endpoint = Endpoint.new(endpoint_params)
    @endpoint.save
    respond_with(@endpoint)
  end

  def update
    @endpoint.update(endpoint_params)
    respond_with(@endpoint)
  end

  def destroy
    @endpoint.destroy
    respond_with(@endpoint)
  end

  def subscribe
    subscription = Subscription.new(endpoint_id: @endpoint.id, consumer_id: current_consumer.id)
    if subscription.save
      notice = "Subscribed to #{@endpoint.title}"
    else
      noitce = "Failed to subscribe"
    end
    redirect_to root_path, notice: notice
  end

  def access
    access = Access.new(endpoint_id: @endpoint.id, consumer_id: current_consumer.id)
    if !current_consumer.reach_limit? and access.save
      notice = "You were accessed #{@endpoint.title}"
    else
      notice = "Failed to access"
    end
    redirect_to root_path, notice: notice
  end

  private
    def set_endpoint
      @endpoint = Endpoint.find(params[:id])
    end

    def endpoint_params
      params.require(:endpoint).permit(:title, :url, :owner_id, :description)
    end
end
