class Endpoint < ActiveRecord::Base
  belongs_to :owner
  has_many :subscriptions

  validates :title, presence: true
  validates :url, presence: true
  validates :owner_id, presence: true
end
