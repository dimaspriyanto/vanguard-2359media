class Subscription < ActiveRecord::Base
  belongs_to :endpoint
  belongs_to :consumer

  validates :endpoint_id, presence: true
  validates :consumer_id, presence: true
end
