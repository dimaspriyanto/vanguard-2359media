class Access < ActiveRecord::Base

  belongs_to :consumer
  belongs_to :endpoint

  validates :consumer_id, presence: true
  validates :endpoint_id, presence: true
  
end
