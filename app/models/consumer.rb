class Consumer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :email, presence: true, uniqueness: true
  validates :password, confirmation: true
  before_create :set_rate_limit

  has_many :accesses
  has_many :subscriptions

  def subscribed?(endpoint)
    Subscription.where(endpoint_id: endpoint.id, consumer_id: self.id).any?
  end

  def monthly_requests
    self.accesses.group_by{|a| a.created_at.beginning_of_month }.map {|k,v| [k.to_s(:short), v.size] }
  end

  def weekly_requests
    self.accesses.group_by{|a| a.created_at.beginning_of_week }.map {|k,v| [k.to_s(:short), v.size] }
  end

  def daily_requests
    self.accesses.group_by{|a| a.created_at.beginning_of_day }.map {|k,v| [k.to_s(:short), v.size] }
  end

  def today_request_count
    self.accesses.where("created_at >= ?", Time.zone.now.beginning_of_day).count
  end

  def reach_limit?
    today_request_count.to_i > self.rate_limit.to_i
  end

  private
  def set_rate_limit
    self.rate_limit = 1000
  end
end
