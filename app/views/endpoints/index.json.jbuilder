json.array!(@endpoints) do |endpoint|
  json.extract! endpoint, :id, :title, :url, :owner_id, :description
  json.url endpoint_url(endpoint, format: :json)
end
