class RemoveUnusedColumns < ActiveRecord::Migration
  def change
    remove_column :accesses, :updated_at
    remove_column :subscriptions, :updated_at
  end
end
