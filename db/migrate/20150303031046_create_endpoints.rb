class CreateEndpoints < ActiveRecord::Migration
  def change
    create_table :endpoints do |t|
      t.string :title
      t.string :url
      t.belongs_to :owner, index: true
      t.text :description

      t.timestamps
    end
  end
end
