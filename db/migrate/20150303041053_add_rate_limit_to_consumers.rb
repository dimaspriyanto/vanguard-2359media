class AddRateLimitToConsumers < ActiveRecord::Migration
  def change
    add_column :consumers, :rate_limit, :integer
  end
end
