class CreateAccesses < ActiveRecord::Migration
  def change
    create_table :accesses do |t|
      t.belongs_to :consumer, index: true
      t.belongs_to :endpoint, index: true

      t.timestamps
    end
  end
end
