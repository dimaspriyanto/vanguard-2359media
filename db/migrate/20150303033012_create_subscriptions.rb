class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.belongs_to :endpoint, index: true
      t.belongs_to :consumer, index: true

      t.timestamps
    end
  end
end
