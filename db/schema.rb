# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150303091121) do

  create_table "accesses", force: true do |t|
    t.integer  "consumer_id"
    t.integer  "endpoint_id"
    t.datetime "created_at"
  end

  add_index "accesses", ["consumer_id"], name: "index_accesses_on_consumer_id"
  add_index "accesses", ["endpoint_id"], name: "index_accesses_on_endpoint_id"

  create_table "consumers", force: true do |t|
    t.string   "full_name"
    t.string   "auth_key"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rate_limit"
  end

  add_index "consumers", ["email"], name: "index_consumers_on_email", unique: true
  add_index "consumers", ["reset_password_token"], name: "index_consumers_on_reset_password_token", unique: true

  create_table "endpoints", force: true do |t|
    t.string   "title"
    t.string   "url"
    t.integer  "owner_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "endpoints", ["owner_id"], name: "index_endpoints_on_owner_id"

  create_table "owners", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "owners", ["email"], name: "index_owners_on_email", unique: true
  add_index "owners", ["reset_password_token"], name: "index_owners_on_reset_password_token", unique: true

  create_table "subscriptions", force: true do |t|
    t.integer  "endpoint_id"
    t.integer  "consumer_id"
    t.datetime "created_at"
  end

  add_index "subscriptions", ["consumer_id"], name: "index_subscriptions_on_consumer_id"
  add_index "subscriptions", ["endpoint_id"], name: "index_subscriptions_on_endpoint_id"

end
