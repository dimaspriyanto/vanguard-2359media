# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

consumer = Consumer.new(email: 'user1@vanguard.com')
consumer.password = 'password'
consumer.password_confirmation = 'password'
consumer.save

owner = Owner.new(email: 'owner1@vanguard.com')
owner.password = 'password'
owner.password_confirmation = 'password'
owner.save


5.times do |i|
  owner = Owner.first
  endpoint = Endpoint.new(title: "Endpoint #{i}", url: "http://my.url/#{i}", owner_id: owner.id)
  endpoint.save
end
